package org.heiner.demo.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleModuleOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleModuleOneApplication.class, args);
	}

}
