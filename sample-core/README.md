# POC for core dependency release process

## Instructions

mvn -B gitflow:release-start -DpushRemote=false
mvn versions:update-properties -DincludeProperties=sample-core.version
mvn versions:commit
git add pom.xml
git commit -m "Update starter dependencies to release version"
mvn -B gitflow:release-finish -DpushRemote=false
git push origin production
git push --tags
mvn versions:update-properties -DincludeProperties=sample-core.version -DallowSnapshots=true
mvn versions:commit
git add pom.xml
git commit -m "Update starter dependencies to snapshot version"
git push origin integration
